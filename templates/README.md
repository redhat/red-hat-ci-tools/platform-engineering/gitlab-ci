# GitLab CI templates

This directory contains templates, which you can reuse by
[including](https://docs.gitlab.com/ee/ci/yaml/includes.html)
them in your `.gitlab-ci.yml`.
We'll use [pre-commit.yml](pre-commit.yml) as an example:

```yaml
include:
  - project: platform-engineering-org/gitlab-ci
    file:
      - templates/pre-commit.yml
```

or if your project is not on gitlab.com:

```yaml
include: https://gitlab.com/platform-engineering-org/gitlab-ci/-/raw/main/templates/pre-commit.yml
```

Then create a job which [extends](https://docs.gitlab.com/ee/ci/yaml/#extends)
the included job.

```yaml
pre-commit:
  extends: .pre-commit
```

You can [override](https://docs.gitlab.com/ee/ci/yaml/includes.html#override-included-configuration-values)
any configuration value from the included job.
For example, you can use a different stage.

Specify your `stages`:

```yaml
stages:
  - build
```

and override the stage used by the job.

```yaml
pre-commit:
  extends: .pre-commmit
  stage: build
```

## Templates

### pre-commit

### build-image

```yaml
include: https://gitlab.com/platform-engineering-org/gitlab-ci/-/raw/main/templates/build-image.yml

build-image:
  extends: .build-image
```

## Adding a new template

- Jobs should be [hidden](https://docs.gitlab.com/ee/ci/jobs/#hide-jobs),
i.e. start with `.`, e.g. `.pre-commit`
