#!/usr/bin/env python3

import os
import re
import sys

from messages import error

if not os.getenv("CI"):
    error("Not running in CI")
    sys.exit(0)

if "platform-engineering-bot" in [
    os.getenv("GITLAB_USER_LOGIN"),
    os.getenv("GITLAB_USER_NAME"),
]:
    print("MR by renovate, ignoring")
    sys.exit(0)

TICKET_PATTERN = re.compile(r"((RHELAI|RHOAIENG|AIPCC)-\d+)", flags=re.MULTILINE)

candidates = []

for varname, is_sufficient in [
    ("CI_COMMIT_DESCRIPTION", False),
    ("CI_COMMIT_MESSAGE", False),
    ("CI_MERGE_REQUEST_TITLE", True),
    ("CI_MERGE_REQUEST_DESCRIPTION", True),
]:
    value = os.getenv(varname)
    if not value:
        error(f"{varname} is not set")
        continue
    matches = TICKET_PATTERN.findall(value)
    if matches:
        candidates.extend(matches)
        print(f"Found {matches} in {varname}={value!r}")
        if is_sufficient:
            break
    else:
        error(f"Did not find {TICKET_PATTERN} in {varname}={value!r}")
else:
    if candidates:
        error(
            f"Found Jira ticket reference(s): {candidates}, but not in the MR description"
        )
    else:
        error("Did not find Jira ticket reference in MR metadata")
    error("Please make sure your MR includes a Jira ticket ID in the title")
    sys.exit(1)
